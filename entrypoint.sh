#!/bin/bash

## Function definition ##

## RUNTIME ##
cd /home/container

if [[ -z "$SPIGOT_VERSION" ]]; then
    SPIGOT_VERSION="1.12.2"
fi

BASEPATH="/opt/spigot"
export SERVER_JARFILE="${BASEPATH}/spigot-${SPIGOT_VERSION}.jar"

# Output Current Java Version
java -version

# Make internal Docker IP address available to processes.
export INTERNAL_IP=`ip route get 1 | awk '{print $NF;exit}'`
echo "IP today: $INTERNAL_IP"

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
eval ${MODIFIED_STARTUP}

# Example startup line: java -Xmx{{SERVER_XMX}}M -Xms{{SERVER_XMS}}M {{EXTRA_JVM}} -jar {{SERVER_JARFILE}} {{EXTRA_APP}}

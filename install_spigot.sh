#!/usr/bin/env bash

SPIGOT_VERSION="$1"

if [[ -z "$SPIGOT_VERSION" ]]; then
    SPIGOT_VERSION="1.12.2"
fi

mkdir -p "spigot/"

SERVER_JARFILE="../spigot/spigot-${SPIGOT_VERSION}.jar"

DIR=$(pwd)
if [[ ! -e "$SERVER_JARFILE" ]]; then
	echo "Setting up BuildTools"
	URL="https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar"
	if [[ ! -d buildtools ]]; then
		mkdir buildtools
	fi
	cd buildtools

	curl -o BuildTools.jar -sSL "${URL}"
	git config --global --unset core.autocrlf
	JAR_EXTRA="${JAR_LINE} --rev=${SPIGOT_VERSION}"
    SPIGOT_JAR="spigot-${SPIGOT_VERSION}.jar"

    if ! java -jar BuildTools.jar ${JAR_EXTRA} ; then
        echo "Build failed!"
        exit 1
    else
        echo "Copying jar file"
        if [[ ! -e ${SERVER_JARFILE} ]]; then
            rm ${SERVER_JARFILE}
        fi
        cp ${SPIGOT_JAR} ${SERVER_JARFILE}
     fi
fi
cd ${DIR}
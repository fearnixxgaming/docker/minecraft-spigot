#!/usr/bin/env bash

if docker build -t "registry.gitlab.com/fearnixxgaming/docker/minecraft-spigot" . ; then
    docker push "registry.gitlab.com/fearnixxgaming/docker/minecraft-spigot:latest"
fi
